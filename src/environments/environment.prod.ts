export const environment = {
  production: true,
  baseUrl: 'https://www.kitshare.com.au/backend/server/',
  breakpoints: {
    desktop: 1200,
    tablet: 768,
  }
};
