import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'kit-logo-container',
  templateUrl: './logo-container.component.html',
  styleUrls: ['./logo-container.component.scss']
})
export class LogoContainerComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
