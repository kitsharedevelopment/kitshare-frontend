export class GearDetails {
        address: any = [];
	    user_gear_desc_id: any = "";
        ks_gear_type_id: any = "";
        ks_manufacturers_id: any = "";
        ks_category_id: any = "";
        gear_name: any = "";
        user_description: any = "";
        gear_description_1: any = "";
        gear_description_2: any = "";
        model_id: any = "";
        app_user_id: any = "";
        serial_number: any = null;
        replacement_value_usd: any = null;
        replacement_value_aud_ex_gst: any = "";
        replacement_value_aud_inc_gst: any = "";
        per_day_cost_usd: any = null;
        per_day_cost_aud_ex_gst: any = "";
        per_day_cost_aud_inc_gst: any = "";
       per_weekend_cost_usd: any = null;
       per_weekend_cost_aud_ex_gst: any = "";
       per_weekend_cost_aud_inc_gst: any = "";
       per_week_cost_usd: any = null;
       per_week_cost_aud_ex_gst: any = "";
       per_week_cost_aud_inc_gst: any = "";
       owners_remark: any = "";
       gear_listing_date: any = null;
       gear_delisting_date: any = null;
       gear_view_count: any = null;
       gear_review_count: any = null;
       gear_star_rating_avg: any = null;
       rating: any = null;
       gear_lending_count: any = null;
       gear_hide_search_results: any = "N";
       ks_user_gear_condition : any = "";
       gear_list_delist_flag: any = "Y";
       is_active: any = "Y";
       create_user: any = "1";
       create_date: any = "2018-02-07";
       update_user: any = "1";
       update_date: any = "2018-02-08";
       model_name: any = "";
       manufacturer_name: any = "";
       gear_category_name: any = "";
       app_user_first_name: any = "";
       app_user_last_name: any = "";
       app_username: any = "";
       user_profile_picture_link: any = "";
       avg_rating: any = [];
       images : Array<GearImageDetails> = [];
       gear_sub_category_name: String ="";
       gear_unavailable: Array<any> = [];
}
export class GearImageDetails {
	user_gear_image_id: any =  "1";
    model_id:any = "1";
    google_360_image_link: any ="";
    gear_display_image: any ="download.jpg";
    gear_display_seq_id: any ="1";
    user_gear_desc_id: any ="1";
    is_active: any ="Y";
    create_user: any ="1";
    create_date: any ="2018-02-07";
    update_user: any =null;
    update_date: any =null
}